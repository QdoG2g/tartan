# Tartan

Tartan is a set of tools and plugins for Clang which aim to improve its
usefulness for developing GLib and GNOME applications and libraries.

The first plugin in this set is a static analysis plugin to
utilise GObject introspection annotations to automatically add attributes to
functions and parameters in C code.

The second plugin utilises `g_return_if_fail()` preconditions to add attributes
to functions and parameters in C code.

## Dependencies

- glib-2.0 ≥ 2.38.0
- gio-2.0 ≥ 2.38.0
- gobject-introspection-1.0 ≥ 1.38.0
- llvm ≥ 13.0
- meson ≥ 0.52.0

## Licensing

Tartan is distributed under the terms of the GNU General Public License,
version 3 or later. See the [COPYING](COPYING) file for details.

## Contact

- Philip Withnall <philip@tecnocode.co.uk>
- Website: http://www.freedesktop.org/software/tartan/
